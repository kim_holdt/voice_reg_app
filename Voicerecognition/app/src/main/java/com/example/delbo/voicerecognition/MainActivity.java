package com.example.delbo.voicerecognition;

import android.app.ProgressDialog;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.os.AsyncTask;
import android.support.v4.content.res.TypedArrayUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Environment;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;
import android.media.MediaRecorder;

import org.apache.commons.lang3.ArrayUtils;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

// Main activity - only one for this app
public class MainActivity extends AppCompatActivity {

    // Initialization
    Button startButton;
    String output, answerFPGA, IP_FPGA;
    int PortNumber;
    TextView answer;
    EditText IPinput, portNumber;
    String AudioSavePathInDevice = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "testAudioRecordingFile1.txt";
    private final String escapeCharacter = ",X";
    short [] audioBuffer;
    final int SAMPLE_RATE = 44100; // The sampling rate
    boolean mShouldContinue; // Indicates if recording / playback should stop
    public static ProgressDialog dialog1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startButton = (Button) findViewById(R.id.startButton);
        IPinput = (EditText) findViewById(R.id.IPinput);
        portNumber = (EditText) findViewById(R.id.portNumber);

        answer = (TextView) findViewById(R.id.answer);

        // Start button pressed - start recording, creating array of samples and send samples
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Start  button disabled
                startButton.setEnabled(false);

                // Reads text from Edit text field
                IP_FPGA = IPinput.getText().toString();

                if(portNumber.getText().toString().equals(""))
                {
                    // Sets text for textView
                    answer.setText("PLEASE FILL IN PORT NUMBER FOR FPGA");
                }
                else PortNumber = Integer.parseInt(portNumber.getText().toString());


                if(IP_FPGA.equals(""))
                {
                    answer.setText("PLEASE FILL IN IP ADDRESS FOR FPGA");
                    startButton.setEnabled(true);
                }
                else {

                    mShouldContinue = true;
                    new recordAudio().execute();
                }

            }

        });

    }

    //Async task for creating the packet/comamnd to be send
    private class createPacketTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            createPacket();
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            //Calls method to send the fil eto the FPGA board
            new Thread(new Runnable() {
                @Override
                public void run()
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run()
                        {
                            //dialog1.dismiss();
                        }
                    });
                }
            }).start();

            new sendFileToServer().execute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    // Method for creating converting the byte[] to string output
    void createPacket() {

        File file = new File(AudioSavePathInDevice);

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {

                e.printStackTrace();
            }
        }

        try {

            // Normalizing the samlpes
            for(short i : audioBuffer) i = (short) Math.round(i/(sqrt(pow(i,2))));

            // IntArray to string
            output = Arrays.toString(audioBuffer).replaceAll("[\\[\\]\\s]", "");

            FileWriter fileWriter = new FileWriter(AudioSavePathInDevice);
            fileWriter.write(output);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // Async task for sending the string using TCP (Socket)
    class sendFileToServer extends AsyncTask<Void, Integer, Integer> {

        // ProcessBox while data is read
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Sending Command...");
            dialog.show();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            Socket s = null;

            try {
                s = new Socket(IP_FPGA, PortNumber);

                OutputStream outstream = s.getOutputStream();
                PrintWriter out = new PrintWriter(outstream);

                // Set Timeout for socket
                s.setSoTimeout(600000);

                String toSend = output;

                out.print(toSend);

                out.flush();

                out.print(escapeCharacter);
                out.flush();

                // Waiting for answer from server (FPGA)
                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));

                answerFPGA = "";


                answerFPGA += in.readLine();

                s.close();
            } catch (Exception e) {
                e.printStackTrace();
                return -1; // failed
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            // Connection fail
            if (result == -1) {
                //System.exit(0);
                answer.setText("CONNECTION ERROR - PLEASE TRY AGAIN");
                startButton.setEnabled(true);
                dialog.dismiss();
            }
            // Checks answer from server (FPGA)
            else
            {

                if (answerFPGA.equals("1"))
                 {
                     answer.setText("You said 'Stop'");
                 }
                 else if (answerFPGA.equals("2"))
                 {
                     answer.setText("You said 'Start'");
                 }
                 else
                 {
                     answer.setText("UNKNOWN COMMAND");
                 }

                // Enables the start button again
                startButton.setEnabled(true);
                dialog.dismiss();
            }
        }
    }

    // Async task for recording audio command from microphone
    class recordAudio extends AsyncTask<Void, Integer, Integer> {

        // ProcessBox while data is read
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage("VOICE COMMAND - Say Move or STOP");
            dialog.show();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

                    // buffer size in bytes
                    int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE,
                            AudioFormat.CHANNEL_IN_MONO,
                            AudioFormat.ENCODING_PCM_16BIT);

                    if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
                        bufferSize = SAMPLE_RATE * 2;
                    }

                    // Setting buffersize - based on minimum bufer size for phone
                    int ourBufferSize = bufferSize*20;

                    //audioBuffer = new byte[ourBufferSize];
                    audioBuffer = new short[ourBufferSize];

                    AudioRecord record = new AudioRecord(MediaRecorder.AudioSource.UNPROCESSED,
                            SAMPLE_RATE,
                            AudioFormat.CHANNEL_IN_MONO,
                            AudioFormat.ENCODING_PCM_16BIT,
                            ourBufferSize);

                    if (record.getState() != AudioRecord.STATE_INITIALIZED) {
                        return;
                    }
                    record.startRecording();


                    long shortsRead = 0;
                    int counter = 0;
                    while (mShouldContinue) {
                        int numberOfShort = record.read(audioBuffer, 0, audioBuffer.length);
                        shortsRead += numberOfShort;
                        counter++;
                        mShouldContinue = false;
                    }
                    record.stop();
                    record.release();

                    AudioTrack at = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                            AudioFormat.ENCODING_PCM_16BIT, audioBuffer.length, AudioTrack.MODE_STREAM);

                    //For playing audio
                    //at.play();
                    //at.write(audioBuffer,0,audioBuffer.length);

                    dialog.dismiss();
                    // Create the packet to be send
                    new createPacketTask().execute();
                }
            }).start();

            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {

        }
    }
}
